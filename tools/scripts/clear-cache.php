<?php
rrmdir(__DIR__ . '/../storage/framework/cache/data/');
rrmdir(__DIR__ . '/../bootstrap/cache/');
function rrmdir($dir)
{
    if (is_dir($dir)) {
        $objects = scandir($dir);
        $removeDir = true;
        foreach ($objects as $object) {
            if ($object != '.' && $object != '..') {
                if (is_dir($dir.'/'.$object) && !is_link($dir.'/'.$object)) {
                    rrmdir($dir.'/'.$object);
                } elseif ($object != '.gitignore') {
                    unlink($dir.'/'.$object);
                } else {
                    $removeDir = false;
                }
            }
        }

        if ($removeDir) {
            rmdir($dir);
        }
    }
}
