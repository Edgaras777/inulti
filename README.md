**Short documentation**

Project with Laravel 8.x

# Docker local environment

### Create ENV
```
cd code
cp .env.example .env
```

### Docker set network (manually, if not created)
```
docker network create --gateway 172.31.1.1 --subnet 172.31.1.0/24 inulti_subnet
```

### Run docker compose
```
cd ..
docker-compose up --build
```

### Add to your /etc/hosts in Linux
```
172.31.1.5  inulti.local
172.31.1.3  inulti.phpmyadmin
```

### Login to container
```
docker-compose exec php_fpm bash
```

### Install project initial data, used after every fetch
```
composer install
```

### Generate Laravel key
```
php artisan key:generate
```

### Fresh project 
```
composer freshdb
```

### Fix permissions
```
chmod -R 777 storage/
```

### Endpoints
```
--------------------------------------

http://inulti.local/api/register
POST Request body: 
{
    "name" : "Test",
    "email" : "test@test.lt",
    "password" : "test@test.lt",
    "sex" : "male"
}
After success login you will get access_token, witch you need to user to reach other routes. 
use it in header as "Authorization" : "Bearer access_token"

--------------------------------------

GET http://inulti.local/api/user -> to recieve current user and all his orders

--------------------------------------

http://inulti.local/api/user/update
POST Request body
{
    "name" : "NewNAme",
    "email" : "test@test2.lt",
    "sex" : "male",
}

--------------------------------------

http://inulti.local/api/user/delete
POST Request body
{
}

--------------------------------------

http://inulti.local/api/order/create
POST Request body: 
{
    "order_number" : "test-123",
    "status" : "COMPLETED",
}
possible statuses : WAITING, COMPLETED, REFUNDED

--------------------------------------

http://inulti.local/api/order/delete
POST Request body
{
    "order_number" : "test-123"
}

--------------------------------------

http://inulti.local/api/order/update
POST Request body
{
    "order_id" : "1",
    "order_number" : "test-123",
    "status" : "",
}

if parameters emty, then this field wouldn't update
--------------------------------------

http://inulti.local/api/login
POST Request body
{
    "name" : "test",
    "password" : "test@test2.lt",
}

--------------------------------------

http://inulti.local/api/logout
POST Request body
{
}

--------------------------------------
```

### phpmyadmin Permission fix
```
if getting error: Wrong permissions on configuration file, should not be world writable!
on http://inulti.phpmyadmin

do this:
docker-compose exec phpmyadmin bash
cd /etc/phpmyadmin/
chmod -R 755 config.inc.php
```