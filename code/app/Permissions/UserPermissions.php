<?php

namespace App\Permissions;

class UserPermissions
{
    const RETRIEVE_USER = 'retrieve_user';
    const RETRIEVE_ALL_USERS = 'retrieve_all_users';
    const CREATE_USER = 'create_user';
    const UPDATE_USER = 'update_user';
    const DELETE_USER = 'delete_user';
}
