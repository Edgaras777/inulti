<?php

namespace App\Permissions;

class OrderPermissions
{
    const RETRIEVE_ORDER = 'retrieve_order';
    const RETRIEVE_ALL_ORDERS = 'retrieve_all_orders';
    const CREATE_ORDER = 'create_order';
    const UPDATE_ORDER = 'update_order';
    const DELETE_ORDER = 'delete_order';
}
