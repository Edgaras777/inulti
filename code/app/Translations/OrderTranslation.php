<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTranslation extends Model
{
    protected $table = 'order_translations';

    public $fillable = [
        //TODO: Same as model's translatedAttributes
    ];
}
