<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTranslation extends Model
{
    protected $table = 'user_translations';

    public $fillable = [
        //TODO: Same as model's translatedAttributes
    ];
}
