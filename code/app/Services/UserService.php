<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @param $validatedField
     *
     * @return mixed
     */
    public function setFields($validatedField): mixed
    {
        foreach ($validatedField as $key => $item) {
            if (!$item) {
                unset($validatedField[$key]);
            }
            if ($item && $key == 'password') {
                $validatedField[$key] = Hash::make($item);
            }
        }

        return $validatedField;
    }
}
