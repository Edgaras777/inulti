<?php

namespace App\Listeners;

use App\Events\DecreaseOrderCount;
use App\Models\User;

class DecreaseOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DecreaseOrderCount  $event
     * @return void
     */
    public function handle(DecreaseOrderCount $event)
    {
        User::where('id', $event->user->id)
        ->decrement('orderscount');
    }
}
