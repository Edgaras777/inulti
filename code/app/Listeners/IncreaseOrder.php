<?php

namespace App\Listeners;

use App\Events\IncreaseOrderCount;
use App\Models\User;

class IncreaseOrder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IncreaseOrderCount  $event
     * @return void
     */
    public function handle(IncreaseOrderCount $event)
    {
        User::where('id', $event->user->id)
            ->increment('orderscount');
    }
}
