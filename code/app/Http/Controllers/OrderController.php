<?php

namespace App\Http\Controllers;

use App\Events\DecreaseOrderCount;
use App\Events\IncreaseOrderCount;
use App\Http\Requests\DeleteOrderRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Order;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * @param OrderRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function create(OrderRequest $request)
    {
        $validatedData = $request->validationData();

        $validatedData['user_id'] = auth()->id();

        $order = Order::create($validatedData);

        event(new IncreaseOrderCount(auth()->user()));

        return response(['order' => $order]);
    }

    /**
     * @param DeleteOrderRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function delete(DeleteOrderRequest $request)
    {
        $validatedData = $request->validationData();

        $order = Order::owner(auth()->id())
            ->where('order_number', $validatedData['order_number'])
            ->delete();

        if ($order) {
            event(new DecreaseOrderCount(auth()->user()));
            return response(['deleted' => $validatedData['order_number']]);
        }

        return response(['error' => 'you do not have this order']);
    }

    /**
     * @param UpdateOrderRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function update(UpdateOrderRequest $request)
    {
        $validatedData = $request->validationData();

        $id = $validatedData['order_id'];
        unset($validatedData['order_id']);

        $order = Order::where('user_id', auth()->id())
            ->where('id', $id)
            ->update($validatedData);

        if ($order) {
            return response(['order' => Order::find($id)]);
        }

        return response(['error' => 'you do not have this order']);
    }
}
