<?php

namespace App\Http\Controllers\Api;

use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Swis\JsonApi\Server\Http\Controllers\Api\BaseApiController;
use Swis\JsonApi\Server\Repositories\RepositoryInterface;

class OrderController extends BaseApiController
{
    /** @var RepositoryInterface $repository */
    protected $repository;

    public function __construct(OrderRepository $repository, Request $request)
    {
        $this->repository = $repository;
        parent::__construct($this->repository, $request);
    }
}
