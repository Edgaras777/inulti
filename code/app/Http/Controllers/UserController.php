<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use App\Services\UserService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Swis\JsonApi\Server\Http\Controllers\Api\BaseApiController;

class UserController extends BaseApiController
{
    protected UserService $userService;

    public function __construct(UserRepository $repository, UserService $userService, Request $request)
    {
        $this->repository = $repository;
        $this->userService = $userService;
        parent::__construct($this->repository, $request);
    }

    /**
     * @return Application|ResponseFactory|Response
     */
    public function index()
    {
        $user = User::with('orders')->where('id', auth()->id())->get();

        return response(['user' => $user]);
    }

    /**
     * @param UserRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function change(UserRequest $request)
    {
        $validatedData = $request->validationData();

        $updateData = $this->userService->setFields($validatedData);

        $user = User::where('id', auth()->id())
            ->update($updateData);

        if ($user) {
            return response(['user' => User::find(auth()->id())]);
        }

        return response(['error' => 'Something is wrong']);
    }

    /**
     * @return Application|ResponseFactory|Response
     */
    public function remove()
    {
        $user = User::find(auth()->id())->delete();

        if ($user) {
            return response(['deleted' => 'User deleted']);
        }

        return response(['error' => 'something wrong, try again']);
    }
}
