<?php

namespace App\Providers;

use App\Events\DecreaseOrderCount;
use App\Events\IncreaseOrderCount;
use App\Listeners\DecreaseOrder;
use App\Listeners\IncreaseOrder;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        IncreaseOrderCount::class => [
            IncreaseOrder::class
        ],
        DecreaseOrderCount::class => [
            DecreaseOrder::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
