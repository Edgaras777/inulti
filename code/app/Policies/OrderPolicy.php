<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Permissions\OrderPermissions;

class OrderPolicy
{
    use HandlesAuthorization;

    //todo: add extra checks. For example: user->id === $requestedItem->owner->id

    public function index(User $user)
    {
        return $user->tokenCan(OrderPermissions::RETRIEVE_ALL_ORDERS);
    }

    public function show(User $user, $requestedItem)
    {
        return $user->tokenCan(OrderPermissions::RETRIEVE_ORDER);
    }

    public function create(User $user)
    {
        return $user->tokenCan(OrderPermissions::CREATE_ORDER);
    }

    public function update(User $user, $requestedItem)
    {
        return $user->tokenCan(OrderPermissions::UPDATE_ORDER);
    }

    public function delete(User $user, $requestedItem)
    {
        return $user->tokenCan(OrderPermissions::DELETE_ORDER);
    }
}
