<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\User;
use App\Permissions\UserPermissions;

class UserPolicy
{
    use HandlesAuthorization;

    //todo: add extra checks. For example: user->id === $requestedItem->owner->id

    public function index(User $user)
    {
        return $user->tokenCan(UserPermissions::RETRIEVE_ALL_USERS);
    }

    public function show(User $user, $requestedItem)
    {
        return $user->tokenCan(UserPermissions::RETRIEVE_USER);
    }

    public function create(User $user)
    {
        return $user->tokenCan(UserPermissions::CREATE_USER);
    }

    public function update(User $user, $requestedItem)
    {
        return $user->tokenCan(UserPermissions::UPDATE_USER);
    }

    public function delete(User $user, $requestedItem)
    {
        return $user->tokenCan(UserPermissions::DELETE_USER);
    }
}
