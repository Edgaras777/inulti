<?php

namespace App\Repositories;

use App\Order;
use Swis\JsonApi\Server\Repositories\BaseApiRepository;

class OrderRepository extends BaseApiRepository
{
    public function getModelName(): string
    {
        return Order::class;
    }
}
