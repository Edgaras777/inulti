<?php

namespace App\Repositories;

use App\User;
use Swis\JsonApi\Server\Repositories\BaseApiRepository;

class UserRepository extends BaseApiRepository
{
    public function getModelName(): string
    {
        return User::class;
    }
}
