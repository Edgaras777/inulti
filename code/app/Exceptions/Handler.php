<?php

namespace App\Exceptions;

use Carbon\Exceptions\InvalidArgumentException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    public function render($request, Throwable $e)
    {
        if ($request->expectsJson()) {
            if ($e instanceof NotFoundHttpException) {
                return response()->json(['errors' => [['status' => '404', 'title'=> 'Route Not Found', 'code' => 'NotFoundHttpException']]], 404);
            }

            if ($e instanceof ThrottleRequestsException) {
                return response()->json(['errors' => [['title'=> 'Per daug bandymų atlikti veiksmą, prašome palaukti ir bandyti vėliau']]], 429);
            }

            if ($e instanceof InvalidArgumentException) {
                return response()->json(['errors' => [['Validation Error', 'detail'=> $e->getMessage(), 'code' => 'InvalidArgumentException']]], 400);
            }

            if ($e instanceof QueryException) {
                return response()->json(['errors' => [['SQL Error', 'detail'=> $e->getMessage(), 'code' => 'QueryException']]], 400);
            }

            if ($e instanceof ValidationException) {
                $status = $e->status;

                $errors = [];
                $input = $request->all();
                foreach ($e->errors() as $key => $val) {
                    foreach ($val as $innerVal) {
                        if (isset($input['data']) and isset($input['data']['type'])) {
                            $errors[] = ['title' => $innerVal, 'source' => ['pointer' => '/data/attributes/' . $key]];
                        } else {
                            $errors[] = ['title' => $innerVal, 'source' => ['parameter' => $key]];
                        }
                    }
                }

                return response()->json(['errors' => $errors], $status);
            }
        }

        return parent::render($request, $e);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }
}
