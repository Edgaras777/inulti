<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class Order extends Model
{
    use Translatable;
    public $translationModel = 'OrderTranslation';

    public $fillable = [
        // TODO: Write down fillables
    ];

    public $hidden = [
            // TODO: Write down hidden
        ];

    public $translatedAttributes = [
        // TODO: Write down translatable values
    ];

    public function getRules($id = null): array
    {
       return [
        //TODO write down rules
       ];
    }
}
