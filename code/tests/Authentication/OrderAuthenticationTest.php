<?php

namespace App\Tests\Authentication;
use Laravel\Passport\Passport;
use App\User;
use App\Order;
use App\Permissions\OrderPermissions;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class OrderAuthenticationTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $baseUrl;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();

        $this->user->givePermissionTo(OrderPermissions::RETRIEVE_ORDER);
        $this->user->givePermissionTo(OrderPermissions::RETRIEVE_ALL_ORDERS);
        $this->user->givePermissionTo(OrderPermissions::CREATE_ORDER);
        $this->user->givePermissionTo(OrderPermissions::UPDATE_ORDER);
        $this->user->givePermissionTo(OrderPermissions::DELETE_ORDER);

        $this->baseUrl = env('API_URL').'/orders/';
        $this->withHeaders(['Accept'=>'application/vnd.api+json']);
    }

    /** @test */
    public function it_creates_an_order_unauthenticated()
    {
        $response = $this->post($this->baseUrl);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_creates_an_order_authenticated()
    {
        Passport::actingAs($this->user);

        $response = $this->post($this->baseUrl);
        $response->assertStatus(201);
    }

    /** @test */
    public function it_updates_an_order_unauthenticated()
    {
        $order = factory(Order::class)->create();

        $response = $this->patch($this->baseUrl.$order->id, $order->toArray());
        $response->assertStatus(401);
    }

    /** @test */
    public function it_updates_an_order_authenticated()
    {
        Passport::actingAs($this->user);

        $order = factory(Order::class)->create();

        $response = $this->patch($this->baseUrl.$order->id, $order->toArray());

        $response->assertStatus(200);
    }

    /** @test */
    public function it_retrieves_an_order_unauthenticated()
    {
        $order = factory(Order::class)->create();

        $response = $this->get($this->baseUrl.$order->id);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_retrieves_an_order_authenticated()
    {
        Passport::actingAs($this->user);

        $order = factory(Order::class)->create();

        $response = $this->get($this->baseUrl.$order->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function it_retrieves_all_orders_unauthenticated()
    {
        factory(Order::class, 3)->create();

        $response = $this->get($this->baseUrl);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_retrieves_all_orders_authenticated()
    {
        Passport::actingAs($this->user);

        factory(Order::class, 3)->create();

        $response = $this->get($this->baseUrl);
        $response->assertStatus(206);
    }

    /** @test */
    public function it_deletes_an_order_unauthenticated()
    {
        $order = factory(Order::class)->create();

        $response = $this->delete($this->baseUrl.$order->id);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_deletes_an_order_authenticated()
    {
        Passport::actingAs($this->user);

        $order = factory(Order::class)->create();

        $response = $this->delete($this->baseUrl.$order->id);
        $response->assertStatus(204);
    }
}
