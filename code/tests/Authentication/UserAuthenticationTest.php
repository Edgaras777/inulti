<?php

namespace App\Tests\Authentication;
use Laravel\Passport\Passport;
use App\User;
use App\User;
use App\Permissions\UserPermissions;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UserAuthenticationTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $baseUrl;

    public function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create();

        $this->user->givePermissionTo(UserPermissions::RETRIEVE_USER);
        $this->user->givePermissionTo(UserPermissions::RETRIEVE_ALL_USERS);
        $this->user->givePermissionTo(UserPermissions::CREATE_USER);
        $this->user->givePermissionTo(UserPermissions::UPDATE_USER);
        $this->user->givePermissionTo(UserPermissions::DELETE_USER);

        $this->baseUrl = env('API_URL').'/users/';
        $this->withHeaders(['Accept'=>'application/vnd.api+json']);
    }

    /** @test */
    public function it_creates_an_user_unauthenticated()
    {
        $response = $this->post($this->baseUrl);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_creates_an_user_authenticated()
    {
        Passport::actingAs($this->user);

        $response = $this->post($this->baseUrl);
        $response->assertStatus(201);
    }

    /** @test */
    public function it_updates_an_user_unauthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->patch($this->baseUrl.$user->id, $user->toArray());
        $response->assertStatus(401);
    }

    /** @test */
    public function it_updates_an_user_authenticated()
    {
        Passport::actingAs($this->user);

        $user = factory(User::class)->create();

        $response = $this->patch($this->baseUrl.$user->id, $user->toArray());

        $response->assertStatus(200);
    }

    /** @test */
    public function it_retrieves_an_user_unauthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->get($this->baseUrl.$user->id);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_retrieves_an_user_authenticated()
    {
        Passport::actingAs($this->user);

        $user = factory(User::class)->create();

        $response = $this->get($this->baseUrl.$user->id);
        $response->assertStatus(200);
    }

    /** @test */
    public function it_retrieves_all_users_unauthenticated()
    {
        factory(User::class, 3)->create();

        $response = $this->get($this->baseUrl);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_retrieves_all_users_authenticated()
    {
        Passport::actingAs($this->user);

        factory(User::class, 3)->create();

        $response = $this->get($this->baseUrl);
        $response->assertStatus(206);
    }

    /** @test */
    public function it_deletes_an_user_unauthenticated()
    {
        $user = factory(User::class)->create();

        $response = $this->delete($this->baseUrl.$user->id);
        $response->assertStatus(401);
    }

    /** @test */
    public function it_deletes_an_user_authenticated()
    {
        Passport::actingAs($this->user);

        $user = factory(User::class)->create();

        $response = $this->delete($this->baseUrl.$user->id);
        $response->assertStatus(204);
    }
}
