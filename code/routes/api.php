<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::post('/login', [AuthController::class, 'login'])->name('login');

Route::middleware('auth:api')->group(function () {

    Route::post('/user/update', [UserController::class, 'change'])->name('user.update');
    Route::post('/user/delete', [UserController::class, 'remove'])->name('user.delete');
    Route::get('/user', [UserController::class, 'index'])->name('user.info');

    Route::post('/order/create', [OrderController::class, 'create'])->name('order.create');
    Route::post('/order/delete', [OrderController::class, 'delete'])->name('order.delete');
    Route::post('/order/update', [OrderController::class, 'update'])->name('order.update');

    Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
});
